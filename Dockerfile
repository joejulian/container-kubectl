FROM registry.gitlab.com/joejulian/docker-arch:latest as build
RUN pacman -Syu --noconfirm binutils fakeroot file grep gzip sudo tar
RUN mkdir kubectl-bin
RUN chown nobody:nobody kubectl-bin
USER nobody
RUN curl -s https://aur.archlinux.org/cgit/aur.git/snapshot/kubectl-bin.tar.gz | tar xzv
WORKDIR kubectl-bin
RUN makepkg -s

FROM registry.gitlab.com/joejulian/docker-arch:latest
COPY --from=build /kubectl-bin/*.xz /tmp
RUN pacman -U --noconfirm /tmp/*.xz
